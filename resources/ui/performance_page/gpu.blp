/* ui/performance_page/gpu.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;
using Adw 1;

template $PerformancePageGpu: Box {
  orientation: vertical;

  WindowHandle {
    child: Box description {
      orientation: vertical;
      margin-top: 10;
      spacing: 7;
      hexpand: true;

      Box {
        spacing: 20;

        Label gpu_id {
          styles [
            "title-1",
          ]

          hexpand: true;
          halign: start;
        }

        Label device_name {
          styles [
            "title-3",
          ]

          halign: end;
          ellipsize: middle;
        }
      }
    };
  }

  Box graph_top {
    orientation: vertical;

    margin-top: 10;

    Box {
      Label {
        styles [
          "caption",
        ]

        hexpand: true;
        halign: start;
        label: _("Overall utilization");
      }

      Label overall_percent {
        styles [
          "caption",
        ]
      }
    }

    $GraphWidget usage_graph_overall {
      vexpand: true;
      hexpand: true;

      height-request: 100;

      base-color: bind-property template.base-color;
      data-set-count: 1;
      scroll: true;
    }
  }

  Box decode_encode_graphs {
    orientation: horizontal;

    margin-top: 10;
    spacing: 10;

    Box encode_graph {
      orientation: vertical;

      Box {
        Label {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("Video encode");
        }

        Label encode_percent {
          styles [
            "caption",
          ]
        }
      }

      $GraphWidget usage_graph_encode {
        vexpand: true;
        hexpand: true;

        height-request: 100;

        base-color: bind-property template.base-color;
        data-set-count: 1;
        scroll: true;
      }
    }

    Box decode_graph {
      orientation: vertical;

      Box {
        Label {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("Video decode");
        }

        Label decode_percent {
          styles [
            "caption",
          ]
        }
      }

      $GraphWidget usage_graph_decode {
        vexpand: true;
        hexpand: true;

        height-request: 100;

        base-color: bind-property template.base-color;
        data-set-count: 1;
        scroll: true;
      }
    }
  }

  Box memory_graph {
    orientation: vertical;

    margin-top: 10;
    margin-bottom: 10;

    Box {
      Label {
        styles [
          "caption",
        ]

        hexpand: true;
        halign: start;
        label: _("Memory usage");
      }

      Label total_memory {
        styles [
          "caption",
        ]
      }
    }

    $GraphWidget usage_graph_memory {
      hexpand: true;

      height-request: 70;

      base-color: bind-property template.base-color;
      data-set-count: 1;
      scroll: true;
    }
  }

  Box details {
    spacing: 20;

    visible: bind-property template.summary-mode inverted;

    Box dynamic_data {
      orientation: vertical;
      spacing: 10;

      Box top_row {
        spacing: 15;

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 230;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Utilization");
          }

          Label utilization {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 200;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Memory usage");
          }

          Label memory_usage {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }

      Box mid_row {
        spacing: 15;

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 230;

          Label {

            styles [
              "caption",
            ]

            halign: start;
            label: _("Clock Speed");
          }

          Label clock_speed {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 230;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Memory speed");
          }

          Label memory_speed {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }

      Box bottom_row {
        spacing: 15;

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 230;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Power draw");
          }

          Label power_draw {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 100;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Temperature");
          }

          Label temperature {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }
    }

    Box system_info {
      spacing: 10;

      Box labels {
        orientation: vertical;
        spacing: 3;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("OpenGL version:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Vulkan version:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("PCI Express speed:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("PCI bus address:");
        }
      }

      Box values {
        orientation: vertical;
        spacing: 3;

        Label opengl_version {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label vulkan_version {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label pcie_speed {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label pci_addr {
          styles [
            "caption",
          ]

          halign: start;
        }
      }
    }
  }

  PopoverMenu context_menu {
    menu-model: context_menu_model;
  }
}

menu context_menu_model {
  section {
    item {
      label: _("Graph _Summary View");
      action: "graph.summary";
    }

    submenu {
      label: _("_View");

      item {
        label: _("CP_U");
        action: "graph.cpu";
      }

      item {
        label: _("_Memory");
        action: "graph.memory";
      }

      item {
        label: _("_Disk");
        action: "graph.disk";
      }

      item {
        label: _("_Network");
        action: "graph.network";
      }

      item {
        label: _("_GPU");
        action: "graph.gpu";
      }
    }
  }

  section {
    item {
      label: _("_Copy");
      action: "graph.copy";
    }
  }
}

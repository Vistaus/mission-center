/* ui/performance_page/disk.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;
using Adw 1;

template $PerformancePageDisk: Box {
  orientation: vertical;

  WindowHandle {
    child: Box description {
      orientation: vertical;
      margin-top: 10;
      spacing: 7;
      hexpand: true;

      Box {
        spacing: 20;

        Label disk_id {
          styles [
            "title-1",
          ]

          hexpand: true;
          halign: start;
        }

        Label model {
          styles [
            "title-3",
          ]

          halign: end;
          ellipsize: middle;
        }
      }

      Box {
        Label {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("Active time");
        }

        Label {
          styles [
            "caption",
          ]

          label: "100%";
        }
      }
    };
  }

  $GraphWidget usage_graph {
    vexpand: true;
    hexpand: true;

    height-request: 120;

    base-color: bind-property template.base-color;
    data-set-count: 1;
    scroll: true;
  }

  Box {
    margin-top: 2;

    Label graph_max_duration {
      styles [
        "caption",
      ]

      hexpand: true;
      halign: start;
      valign: start;
    }

    Label {
      styles [
        "caption",
      ]

      valign: start;
      label: "0";
    }
  }

  Box {
    margin-top: 10;

    Label {
      styles [
        "caption",
      ]

      hexpand: true;
      halign: start;
      label: _("Disk transfer rate");
    }

    Label max_y {
      styles [
        "caption",
      ]
    }
  }

  $GraphWidget disk_transfer_rate_graph {
    hexpand: true;
    height-request: 50;

    base-color: bind-property template.base-color;
    data-set-count: 3;
    scroll: true;
    auto-scale: true;
    auto-scale-pow2: true;
  }

  Box {
    margin-bottom: 10;

    Label {
      styles [
        "caption",
      ]

      hexpand: true;
      halign: start;
      label: bind-property graph_max_duration.label;
    }

    Label {
      styles [
        "caption",
      ]

      label: "0";
    }
  }

  Box details {
    spacing: 20;

    visible: bind-property template.summary-mode inverted;

    Box dynamic_data {
      orientation: vertical;
      spacing: 10;

      Box top_row {
        spacing: 15;

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 120;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Active time");
          }

          Label active_time {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 120;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Average response time");
          }

          Label avg_response_time {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }

      Box bottom_row {
        spacing: 15;

        Box {
          spacing: 5;

          Picture legend_read {
            can-shrink: false;
            content-fit: scale_down;
          }

          Box {
            orientation: vertical;
            spacing: 3;
            width-request: 112;

            Label {

              styles [
                "caption",
              ]

              halign: start;
              label: _("Read speed");
            }

            Label read_speed {
              styles [
                "title-4",
              ]

              halign: start;
            }
          }
        }

        Box {
          spacing: 5;

          Picture legend_write {
            can-shrink: false;
            content-fit: scale_down;
          }

          Box {
            orientation: vertical;
            spacing: 3;
            width-request: 120;

            Label {
              styles [
                "caption",
              ]

              halign: start;
              label: _("Write speed");
            }

            Label write_speed {
              styles [
                "title-4",
              ]

              halign: start;
            }
          }
        }
      }
    }

    Box system_info {
      spacing: 10;

      Box labels {
        orientation: vertical;
        spacing: 3;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Capacity:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Formatted:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("System disk:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Type:");
        }
      }

      Box values {
        orientation: vertical;
        spacing: 3;

        Label capacity {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label formatted {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label system_disk {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label disk_type {
          styles [
            "caption",
          ]

          halign: start;
        }
      }
    }
  }

  PopoverMenu context_menu {
    menu-model: context_menu_model;
  }
}

menu context_menu_model {
  section {
    item {
      label: _("Graph _Summary View");
      action: "graph.summary";
    }

    submenu {
      label: _("_View");

      item {
        label: _("CP_U");
        action: "graph.cpu";
      }

      item {
        label: _("_Memory");
        action: "graph.memory";
      }

      item {
        label: _("_Disk");
        action: "graph.disk";
      }

      item {
        label: _("_Network");
        action: "graph.network";
      }

      item {
        label: _("_GPU");
        action: "graph.gpu";
      }
    }
  }

  section {
    item {
      label: _("_Copy");
      action: "graph.copy";
    }
  }
}

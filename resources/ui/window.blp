/* ui/window.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;
using Adw 1;

template $MissionCenterWindow : Adw.ApplicationWindow {
  default-width: 600;
  default-height: 300;

  content: Box {
    orientation: vertical;

    Adw.HeaderBar header_bar {
      title-widget: Stack header_stack {
        transition-type: crossfade;

        StackPage {
          name: "view-switcher";
          child: Adw.ViewSwitcher title {
            halign: center;
            policy: wide;
            stack: stack;
          };
        }

        StackPage {
          name: "search-entry";
          child: SearchEntry search_entry {
            width-request: 400;
            placeholder-text: _("Type a name or PID to search");
          };
        }
      };

      [end]
      Box {
        ToggleButton search_button {
          styles [
            "flat"
          ]
          action-name: "win.toggle-search";
          icon-name: "system-search-symbolic";
        }

        MenuButton {
          icon-name: "open-menu-symbolic";
          menu-model: primary_menu;
        }
      }
    }

    Box {
      orientation: horizontal;

      Adw.ViewStack stack {
        hexpand: true;
        vexpand: true;

        Adw.ViewStackPage {
          name: "performance-page";
          icon-name: "speedometer-symbolic";
          title: _("Performance");
          child: $PerformancePage performance_page {
            summary-mode: bind-property header_bar.visible bidirectional inverted;
          };
        }

        Adw.ViewStackPage {
          name: "apps-page";
          icon-name: "overlapping-windows-symbolic";
          title: _("Apps");
          child: $AppsPage apps_page {
          };
        }
      }
    }
  };
}

menu primary_menu {
  section {
    item {
      label: _("_Preferences");
      action: "app.preferences";
    }

    item {
      label: _("_About MissionCenter");
      action: "app.about";
    }
  }

  section {
    item {
      label: _("_Quit");
      action: "app.quit";
    }
  }
}
